class UnitTester {
    constructor(noToCheck){
        this.noToCheck=noToCheck;
    }
    toBe(value) {
        if (value===this.noToCheck) {
            console.log("test passed and the output is "+this.noToCheck)
        }
        else {
            console.log("test is failed output is not the same")
        }
    }
}

function expect (noToCheck) {
    return new UnitTester (noToCheck)
}
export default expect